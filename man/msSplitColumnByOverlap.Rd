% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/msSplitColumnByOverlap.R
\name{msSplitColumnByOverlap}
\alias{msSplitColumnByOverlap}
\title{find genes overlapping with other genomic features (Martin Morgan)}
\usage{
msSplitColumnByOverlap(query, subject, column = "ENTREZID", ...)
}
\arguments{
\item{query}{Granges, eg. gene coordinates}

\item{subject}{Granges, genomic feature coordinates, e.g. copy number coordinates}

\item{column}{a column in the metadata from query Granges,
'column' needs to match 'column' in the geneRanges() function, default='ENTREZID'}
}
\description{
Find and return query rows (eg. genes) that overlap with subject (eg copy number regions). Returns a CharacterList of the length 
of subject with entries from "column" from the metadata of query for all overlapping ranges.
}
\details{
The basic idea behind splitByOverlap is that we can find which gene coordinates overlap
which copy number variant coordinates, and then split the column of gene identifiers into
lists corresponding to the regions of overlap
https://support.bioconductor.org/p/67118/
}
\examples{

(subjectGR <- GRanges(seqnames=Rle(rep(c("chr1", "chr2"), each=5)),
ranges=IRanges(start = c(1,5,10,15,20,1,5,10,15,20), width=10),
mcol=DataFrame(subjectRow=paste0("subject_", 1:10))
))
(queryGR <- GRanges(seqnames=Rle(c("chr1", "chr1", "chr2")),
                   ranges=IRanges(start = c(1, 7, 7), width=c(10,1,10)),
                   mcol=DataFrame(queryRow=paste0("query", 1:3))
))

msSplitColumnByOverlap(queryGR, subjectGR, column="mcol.queryRow")
}
\keyword{Martin}
\keyword{Morgan}
\keyword{find}
\keyword{overlaps,}
