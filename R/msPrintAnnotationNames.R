#' print names next to the annotation rows/columns
#'
#'
#'
#' @param annList list of heatmap annotations (HeatmapAnnotation object)
#' @param forColumns should annotation names be added to columns (if annotations are on left/right sided)
#' @param nrOfHeatmaps specify the number of heatmaps, which were plotted for correct positioning
#' @export
#' @examples
#'
#'

msPrintAnnotationNames <- function(annList=annList, forColumns=FALSE, nrOfHeatmaps=2){
  stopifnot(is.list(annList))
  if(!is.null(annList$ha_top)){
    annListNames <- names(annList$ha_top@anno_list)
  }else{
    annListNames <- NULL
  }
  if(!is.null(annList$ha_bottom)){
    annListNames <- c(annListNames, names(annList$ha_bottom@anno_list))
  }
  if(!is.null(annListNames)){
    for(an in annListNames){
      if(forColumns){
        decorate_annotation(an, {
          # annotation names below the columns
          grid.text(an, unit(0, "npc") + unit(3, "mm"), unit(0,"npc")+unit(-2,"mm"), default.units = "npc",
                    just = "right", gp = gpar(fontsize= 10), rot=90)
          # annotation names on the left
          #grid.text(an, unit(0, "npc") - unit(2, "mm"), 0.5, default.units = "npc", just = "right")
        })
      }else{
        decorate_annotation(an, {
          # annotation names on the right
          grid.text(an, unit(1*nrOfHeatmaps, "npc") + unit(2*nrOfHeatmaps, "mm"), unit(0.5,"npc"),
                    default.units = "npc",
                    just = "left", gp = gpar(fontsize= 10))
          # annotation names on the left
          #grid.text(an, unit(0, "npc") - unit(2, "mm"), 0.5, default.units = "npc", just = "right")
        })
      }
    }
  }
}
