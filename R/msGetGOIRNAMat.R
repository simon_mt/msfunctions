#' get expression values for genes of interest from expression matrix
#'
#' extracts gene expression values for a given vector of genes of interest
#'
#' @param exprMat expression matrix with n genes and m samples
#' @param goi genes for which the expression values should be extracted from the matrix
#' @param normFold if TRUE, the log2-fold change for the expression values is calculated, parameters
#' can be passed on to msNormFoldChange
#' @param keepGOIOrder should rows in exprMat have same order as genes in goi, default=T
#' @keywords get expression, expression, extract expression, goi
#' @export
#' @examples
#'

msGetGOIRNAMat <- function(exprMat, goi=NULL, normFold=TRUE, keepGOIOrder=TRUE, verbose=TRUE, ...){
  library(dplyr)
  if(is.null(goi)){
    goi <- unique(rownames(exprMat))
  }
  geneNames <- rownames(exprMat)
  goiMask <- geneNames %in% goi
  exprMat <- exprMat[goiMask,, drop=FALSE]
  geneNamesNew <- geneNames[goiMask]
  if(any(duplicated(geneNamesNew))){
    if(verbose){
      warning("Expression matrix contains duplicated rownames. Summing up counts from rows with same name.")
    }
    # exprMat <- as.data.frame(exprMat)
    # exprMat <- stats::aggregate(exprMat, list(gene=geneNames[goiMask]), FUN = sum)
    # genesAggregated <- exprMat$gene
    # rownames(exprMat) <- genesAggregated
    # exprMat <- exprMat[,-1]
    exprMat <- exprMat %>%
      as.data.frame %>%
      dplyr::group_by(summarizedGenes=!!geneNamesNew) %>%
      summarise_all(list(sum)) %>%
      as.data.frame %>%
      tibble::column_to_rownames('summarizedGenes')
  }else{
    exprMat <- as.data.frame(exprMat)
    rownames(exprMat) <- geneNamesNew
  }

  #exclude all goi, which cannot be found in the gene expression data
  notFound <- goi[!goi %in% geneNames]
  if(verbose){
    lengthNotFound <- length(notFound)
    if(lengthNotFound>100){
      notFound <- c(notFound[1:100], "... ")
    }
    if(lengthNotFound>0){
      message(paste0("\n\nNo expression values found for ",
                 lengthNotFound, " out of ", length(goi)," genes:\n\n"))
      message(notFound)
    }else{
      message("\n\nExpression values found for all ", length(goi), " genes.\n")
    }
  }
  goi <- goi[goi %in% geneNames]
  if(is.null(goi)){
    return(NULL)
  }else{
    goiexprMat <- exprMat[rownames(exprMat) %in% goi,, drop=FALSE]
  }

  if(normFold){
    goiexprMat <- msNormFoldChange(goiexprMat, ...)
  }
  if(keepGOIOrder){
    goiexprMat <- goiexprMat[match(goi, rownames(goiexprMat)), ]
  }
  goiexprMat
}
