% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/msPrintAnnotationNames.R
\name{msPrintAnnotationNames}
\alias{msPrintAnnotationNames}
\title{print names next to the annotation rows/columns}
\usage{
msPrintAnnotationNames(annList = annList, forColumns = FALSE,
  nrOfHeatmaps = 2)
}
\arguments{
\item{annList}{list of heatmap annotations (HeatmapAnnotation object)}

\item{forColumns}{should annotation names be added to columns (if annotations are on left/right sided)}

\item{nrOfHeatmaps}{specify the number of heatmaps, which were plotted for correct positioning}
}
\description{
print names next to the annotation rows/columns
}
\examples{


}
