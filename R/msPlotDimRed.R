#' @name msPlotDimRed
#' @rdname msPlotDimRed
#'
#' @title plot dimensionality reduction results from PCA, t-SNE or umap
#'
#' @description  plot dimensionality reduction results from PCA, t-SNE or umap
#' @param scoresDf data.frame containing columns defined in comp1col and comp2col
#' @param comp1col column name or number that contains x component to be plotted. Default is 1.
#' @param comp2col column name or number that contains y component to be plotted. Default is 2.
#' @param type one of "pca","tsne" and "umap"
#' @param labels labels as character vector for the scores to be plotted
#' @param noLegend should legend be shown or not, default=FALSE
#' @examples

#' msPlotDimRed(LMS_PCA$x, comp1 = "PC1", comp2= "PC5", labels = LMSsamples)
#' 
#' msPlotDimRed(umapTest, comp1col = 1, comp2col = 2, labels = LMSsamples)
#' msPlotDimRed(LMS_PCA, comp1col = 1, comp2col = 2, labels = LMSsamples)
#' msPlotDimRed(tsne, comp1 = 1, comp2= 2, labels = LMSsamples)

msPlotDimRed <- function(DimRedObj, 
                         comp1col=1, 
                         comp2col=2, 
                         type="pca",
                         labels=NULL,
                         noLegend=F){
  if(class(DimRedObj)=="prcomp"){
    scoresDf <- as.data.frame(DimRedObj$x)
  }else if(class(DimRedObj)=="umap"){
    scoresDf <- as.data.frame(DimRedObj$layout)
  }else if(class(DimRedObj)=="list"){
    if("Y" %in% names(DimRedObj)){
      scoresDf <- as.data.frame(DimRedObj$Y)
    }else{
      stop("DimRedObj is list. Should be umap, tsne or prcomp result or data.frame.")
    }
  }else if(class(DimRedObj) %in% c("data.frame", "matrix")){
    scoresDf <- as.data.frame(DimRedObj)
  }else{
    stop(paste0("DimRedObj is ", class(DimRedObj), ". Should be umap, tsne or prcomp result or data.frame."))
  }
  
  if(is.character(comp1col)){
    stopifnot(all(c(comp1col, comp2col) %in% colnames(scoresDf)))
    comp1 <- scoresDf[, colnames(scoresDf)==comp1col]
    comp2 <- scoresDf[, colnames(scoresDf)==comp2col]
  }else if(is.numeric(comp1col)){
    stopifnot(all(c(comp1col, comp2col) <= ncol(scoresDf)))
    comp1 <- scoresDf[, comp1col]
    comp2 <- scoresDf[, comp2col]
  }
  
  plotColors <- msPickSampleColors(labels)
  plotShapes <- msPickSampleColors(labels, symbols = T)
  
  
  if(type=="pca" | type=="tsne"){
    p <- ggplot(scoresDf, aes(x=comp1, y=comp2, color=labels, shape=labels))  + 
      geom_point(size=2.5) + 
      scale_color_manual(values=plotColors) +
      labs(x=comp1col, y=comp2col, color="group", shape="group") +
      scale_shape_manual(values=plotShapes)
    if(noLegend){
      p <- p + theme(legend.position="none")
    }
    p
  }else if(type=="umap"){
    xylim = range(scoresDf)
    xylim = xylim + ((xylim[2]-xylim[1])*pad)*c(-0.5, 0.5)
    if (!add) {
      par(mar=c(0.2,0.7,1.2,0.7), ps=10)
      plot(xylim, xylim, type="n", axes=F, frame=F)
      rect(xylim[1], xylim[1], xylim[2], xylim[2], border="#aaaaaa", lwd=0.25)
    }
    points(scoresDf[,1], scoresDf[,2], col=plotColors[as.integer(labels)],
           cex=cex, pch=pch)
    mtext(side=3, main, cex=cex.main)
    
    labels.u = unique(labels)
    legend.pos = "topright"
    legend.text = as.character(labels.u)
    if (add) {
      legend.pos = "bottomright"
      legend.text = paste(as.character(labels.u), legend.suffix)
    }
    legend(legend.pos, legend=legend.text,
           col=plotColors[as.integer(labels.u)],
           bty="n", pch=pch, cex=cex.legend)
  }
}
