#' Pick distinguishable colors for factors
#'
#' Returns a vector of colors with names according to input vector levels.
#'
#' @param samples vector containing the names, for which colours should be assinged
#' @param sarcColors if TRUE, assign fixed colors for sarcoma subtypes
#' @param symbols if TRUE, assign numbers instead of colors (useful if groups should be encoded by symbols)
#' @keywords colors ComplexHeatmap
#' @export
#' @examples
#' msPickSampleColors(c("a","d","c","a"))
#' msPickSampleColors(c("a","d","c","a"), symbols=TRUE)

msPickSampleColors <- function(samples, sarcColors=FALSE, abesColours=FALSE, tumorSite=FALSE, symbols=FALSE, na_col='mistyrose'){
  if(symbols){
    myColors <- c(0, 4, 2, 7,10,
                  16, 12,13, 14, 11,
                  8, 1, 15,17,9,
                  23,6,20, 5,18,
                  19,21,22,24,3)
  }else{
    library(RColorBrewer)
    library(grDevices)
    #read colors from file to allow altering (corresponing to r_colors_mspicksamplecolors.svg)
    myColors = paste0('#', toupper(readLines('/abi/data2/simonma/projects/maltesFunctions/myColors.txt')))
    #myColors = c(myColors, rainbow(200))
    #myColors = c('#E41A1C','#0000FF','#4DAF4A','#FF8600','#FF00FF','#00FFFF','#FFFF00',
    #               '#00FF00','#D40055','#49006A','#FED976','#984EA3','#377EB8',
    #               '#949305','#66C2A5','#00441B','#863100','#666666','#000000','#BCEE68',
    #               '#D96CA5','#B8B3B3','#BF812D','#00B9FF','#800026','#FFBF00','#FF8080', rainbow(200))

    # qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
    # col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
    # greys <- colorRampPalette(c("grey20", "grey80"))
    # myColors <- c(brewer.pal(9, "Set1")[-6], brewer.pal(8, "Set2")[c(1,7)],
    #               brewer.pal(9, "YlOrRd")[9], brewer.pal(9,"RdPu")[9], brewer.pal(9,"BuGn")[9],
    #               "darkolivegreen2", "cyan", "green", "magenta", "gold", "pink", "black",
    #               brewer.pal(9,"BrBG")[2:4], brewer.pal(9,"YlOrRd")[c(1,3)], "yellow", "blueviolet",
    #               "cyan4", "burlywood4", "deeppink3", "lightcyan2", "mediumspringgreen",
    #               "darkblue", "orangered3", "royalblue", "plum","turquoise4", "yellowgreen",
    #               "yellow4", "thistle", "lightslateblue", greys(6), rainbow(200))
  }
  abundantSubtypes <- as.character(unique(samples))
  sampleLevels <- length(abundantSubtypes)
  if(sampleLevels > length(myColors)){
    warning(sprintf("Number of levels is %i, but only %i distinguishable colors are available. Now interpolating colors!", sampleLevels, length(myColors)))
    myColors = grDevices::colorRampPalette(myColors)(sampleLevels)
  }
  if(!is.logical(sarcColors)){
    if(sarcColors=="TCGA"){
    myColors <- structure(paste0("#", toupper(c("ffa100ff", "d16528ff", "00bd9eff",
                                                "ff0000ff", "0094c8ff", "ba68e7ff",
                                                "fffb06ff", "e88314ff"))),
                          names=c("STLMS", "ULMS", "DDLPS", "UPS", "MFS", "SS", "MPNST", "LMS"))
    }
  }else if(sarcColors){
    if(symbols){
      myColors <- c(0, 4, 2, 7,10,
                    16, 12,13, 14, 11,
                    8, 1, 15,17,9,
                    23,6,20, 5,18,
                    19,21,22,24,3)
    }
    #define unique names for colors to avoid changing colors
    names(myColors) <- c("ASPS", "Chordoma", "CSA", "EPS", "GIST",
                         "LMS", "MLS", "NOS", "OS", "RMS",
                         "SCS", "SFT", "SS", "DDLS", "desmoid",
                         "giant cell UPS/MFH", "MFS", "MPNST", "LS", "UPS/PMFH",

                         "Whole blood","PBMC", "Granulocytes","CD4+ T cells", "CD8+ T cells","CD14+ Monocytes",
                         "CD19+ B cells", "CD56+ NK cells", "Neutrophils", "Eosinophils",

                         "PNET", "FDCS", "FMS", "EHE", "ASA", "EWS", "FS", "RCT",
                         "CCS", "PLS", "LS", "IMT", "WDLS", "ULMS", "PRMS")
    #quick fix: same color should be used for DDLS and DDLPS, UPS/PMFH and UPS
    myColors <- c(myColors, "DDLPS"="darkolivegreen2", "UPS"="black")
    myColors <- myColors[names(myColors) %in% abundantSubtypes]
  } else if(abesColours){
    myColors <- structure(toupper(c("#ffa100", "#00bd9e", "#0094c8","#ff0000", "#ba68e7", "#fffb06")),
                            names=c("LMS", "DDLPS", "MFS", "UPS", "SS", "MPNST"))
    if(length(abundantSubtypes)>length(myColors)){
      myColorsAdditional  <- c(brewer.pal(9, "Set1")[-6], brewer.pal(8, "Set2")[c(1,7)],
                                brewer.pal(9, "YlOrRd")[9], brewer.pal(9,"RdPu")[9], brewer.pal(9,"BuGn")[9],
                                "darkolivegreen2", "cyan", "green", "magenta", "gold", "pink", "black",
                                brewer.pal(9,"BrBG")[2:4], brewer.pal(9,"YlOrRd")[c(1,3)], "yellow", "blueviolet",
                                "cyan4", "burlywood4", "deeppink3", "lightcyan2", "mediumspringgreen",
                                "darkblue", "orangered3", "royalblue", "plum","turquoise4", "yellowgreen",
                                "yellow4", "thistle", "lightslateblue", greys(6), rainbow(200))
      #define unique names for colors to avoid changing colors
      names(myColorsAdditional) <- c("ASPS", "Chordoma", "CSA", "EPS", "GIST",
                           "LMS", "MLS", "NOS", "OS", "RMS",
                           "SCS", "SFT", "SS", "DDLS", "desmoid",
                           "giant cell UPS/MFH", "MFS", "MPNST", "LS", "UPS/PMFH",

                           "Whole blood","PBMC", "Granulocytes","CD4+ T cells", "CD8+ T cells","CD14+ Monocytes",
                           "CD19+ B cells", "CD56+ NK cells", "Neutrophils", "Eosinophils",

                           "PNET", "FDCS", "FMS", "EHE", "ASA", "EWS", "FS", "RCT",
                           "CCS", "PLS", "LS", "IMT", "WDLS", "ULMS", "PRMS")
      myColorsAdditional <- myColorsAdditional[!names(myColorsAdditional) %in% names(myColors)]
      myColors <- c(myColors, myColorsAdditional)
    }
  }else if(tumorSite){
    myColors <- structure(c("khaki1", "gold4", "#56B4E9", "blue", "#009E73", "firebrick1", "#999999", "#CC79A7", "mistyrose"), #c(RColorBrewer::brewer.pal(8, "Set1"),"mistyrose"),
              names=c("Retroperitoneum/Upper abdominal", "Lower abdominal/Pelvic", "Lower Extremity", "Upper Extremity",
                      "Gynecological", "Superficial Trunk", "Head and Neck", "Chest", "NA"))
  }else{
    names(myColors) <- abundantSubtypes
    myColors <- myColors[1:sampleLevels]
  }
  #quick fix: define mistyrose for 'NA' values
  if(!is.null(na_col)){
    myColors <- c("NA"=na_col, myColors)[!duplicated(c('NA',names(myColors)))]
  }
  return(myColors)
}

