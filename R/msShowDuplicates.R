#' @name msShowDuplicates
#' @rdname msShowDuplicates
#'
#' @title show duplicated rows in a data.frame or duplicated values in vector
#'
#' @description  query biological databases to find enriched gene sets
#' @param obj data.frame or vector to check
#' @param columnToCheck if obj is data.frame: column which should be checked for duplicates, defaults to rownames of the df
#' @param maxCols if obj is data.frame: maximum number of duplicated rows to show, default=6
#' @export
#' @examples
#'  testdf <- data.frame(A=c(3,6,8,6,3), B=letters[1:5])
#'  msShowDuplicates(testdf, columnToCheck = 'A')

msShowDuplicates <- function(obj, returnIndex=FALSE, columnToCheck='rownames', maxCols=6){
  if(is.vector(obj)){
    obj_duplicated <- obj[duplicated(obj)]
    if(returnIndex){
      return(which(obj %in% obj_duplicated))
    }
    return(obj[obj %in% obj_duplicated])
  }
  if(is_tibble(tss_tb_small)){
    obj <- as.data.frame(obj)
  }
  colsToShow <- ifelse(ncol(obj)< maxCols, ncol(obj), maxCols)
  if(columnToCheck=='rownames'){
    duplRows <- rownames(obj)[duplicated(rownames(obj))]
    message(sprintf('%i rows have duplicates', length(duplRows)))
    if(returnIndex){
      return(which(rownames(obj) %in% duplRows))
    }
    return(obj[rownames(obj) %in% duplRows, 1:colsToShow])
  }else if(columnToCheck %in% colnames(obj)){
    duplRows <- obj[,columnToCheck][duplicated(obj[,columnToCheck])]
    message(sprintf('%i rows have duplicates', length(duplRows)))
    if(returnIndex){
      return(which(obj[, columnToCheck] %in% duplRows))
    }
    return(obj[obj[, columnToCheck] %in% duplRows, 1:colsToShow])
  }else{
    stop(sprintf('%s not found in obj', columnToCheck))
  }
}
