#' group-wise heatmap
#'
#' plots a heatmap optionally sorted by entities or another annotation column
#' (and clustered within these) or for one entity (if sample_subset is defined)
#' make sure sample_subset is not defined globally
#' Requires methMat, exprMat and phenoDf samples to be in the same order!
#' Returns a vector of colors with names according to input vector levels.
#' ISSUES: 
#' *cluster_columns=F leads to unexpected behaviour
#' *roundVals leads to unexpected results
#' *arguments na_col, annotation_size_cm, legend_position not working yet
#' *example2 fails because of cluster_within_groups_by and sample_subset are same column
#' 
#' @param assay1_df matrix/data.frame, which is used to cluster on (eg. methylation...)
#' @param assay2_df optional matrix/data.frame with same dimensions and observations as assay1_df (eg. expression...)
#' @param pheno_df optional data.frame with sample annotations, which can be used to create heatmap annotations (defined with custom_annotations_list)
#' @param custom_annotations_list optional named list passed to msHeatmapAnnotations() to create heatmap annotations
#' @param heatmap_parameters_list optional named list with parameters passed to Heatmap() to alter Heatmap behaviour
#' @param cluster_within_groups_by optionally specify a column from pheno_df, which is used for grouping the samples before clustering
#' @param sample_subset optionally specify a list containing the name of a column from pheno_df, and a vector of values, which represents the values from this column to keep
#' @export
#' @examples
#'
#' #generate expression and methylation matrices
#' testMethMat <- matrix(sample(seq(0,1,0.05), 80, replace = T), ncol=20, dimnames=list(paste0("cg", 1:4)))
#' testExprMat <- matrix(rexp(80, rate=.1), ncol=20, dimnames=list(paste0("gene", 1:4)))
#' testExprMat <- msNormFoldChange(as.data.frame(testExprMat))
#' 
#' #generate some annotations for the matrix
#' set.seed(1)
#' testAnDf <- data.frame(type=sample(LETTERS[1:4],20,replace = T),
#'                     proportion=runif(n=20, min=0, max=1),
#'                     toBeOrNot=sample(c("yes","no"), 20, replace = T),
#'                     bigValues=sample(1:100, 20, replace=T))
#'
#' #make sure that the sample names are matching in their sample names and order
#' colnames(testMethMat) <- colnames(testExprMat) <- rownames(testAnDf) <- paste0("sample_", 1:20)
#'
#' #list containing arguments passed to msHeatmapAnnotations() function call
#' #Values that can be passed to the list of custom annotations (each again as a list):
#' #annot (must), levels, binNames, annotCol, roundVals (TODO: check roundVals)
#' custom_anno_list <- list(
#'   typeDefaultStyle=list(annot="type"),
#'   typeOwnStyle=list(annot="type",
#'                     annotCol=msPickSampleColors(testAnDf$type)),
#'   proportion=list(annot="proportion",
#'                   levels=seq(0, 1, 0.2),
#'                   annotCol=c("white","blue"),
#'                   roundVals=1),
#'   'big values'=list(annot="bigValues",
#'                     levels=3,
#'                     binNames=c("low", "medium","high"),
#'                     annotCol="Set1"),
#'    'yes_or_no'=list(annot='toBeOrNot'))
#'
#' #example 1
#' heatmapList <- ms_plot_heatmap_per_subtype(
#'  assay1_df = testMethMat,
#'  assay2_df = testExprMat,
#'  pheno_df = testAnDf,
#'  custom_annotations_list = custom_anno_list,
#'  sample_subset = list("type", c("A", "B")))
#'
#' draw(heatmapList$assay1_heatmap + heatmapList$assay2_heatmap)
#'
#' #example 2
#' heatmapList <- ms_plot_heatmap_per_subtype(
#'  assay1_df = testMethMat,
#'  assay2_df = testExprMat,
#'  pheno_df = testAnDf,
#'  custom_annotations_list = custom_anno_list,
#'  heatmap_parameters_list = list(
#'    show_column_names=T, 
#'    annotation_position = TRUE,
#'    annotation_size_cm = 15,
#'    annotation_na_col = 'pink'
#'    ),
#'  cluster_within_groups_by = "type",
#'  sample_subset = list("type", c("A","C","D")))
#'
#' draw(heatmapList$assay1_heatmap %v% heatmapList$assay2_heatmap)
#'
#' #example 3
#' heatmapList <- ms_plot_heatmap_per_subtype(
#'   assay1_df = testMethMat,
#'   assay2_df = NULL,
#'   pheno_df = testAnDf,
#'   custom_annotations_list = custom_anno_list,
#'   heatmap_parameters_list = list(width=unit(12,"cm"),
#'                           height=unit(6, "cm"),
#'                           heatmap_legend_param = list(
#'                             title="modified legend title",
#'                             title_gp=gpar(fontsize=5), #title font size
#'                             at=seq(0,8,2), #where are legend ticks drawn
#'                             labels=c(0, "two", 4, 6, "highest")), #custom legend labels
#'                           col=colorRamp2(c(0,0.2,0.8,1), c("black","blue", "white", "yellow")),
#'                           cluster_rows=FALSE,
#'                           column_title="my title"),
#'   cluster_within_groups_by = "type")
#'
#' draw(heatmapList$assay1_heatmap, heatmap_legend_side='left', annotation_legend_side='bottom')
#'
#'
#'#Values that can be passed to heatmap options with heatmap_parameters_list:
#'#Additional values (passed on to msHeatmapAnnotations() to control heatmap annotation position, height, and colour of na values): 
#' #annotations_position = TRUE,  
#' #annotation_size_cm = 0.2, 
#' #annotation_na_col = 'grey95'
#'#Default values from Heatmap() function that can be used:
#'#col,
#'#name,
#'#na_col = "grey",
#'#color_space = "LAB",
#'#rect_gp = gpar(col = NA),
#'#cell_fun = NULL,
#'#row_title = character(0),
#'#row_title_side = c("left", "right"),
#'#row_title_gp = gpar(fontsize = 14),
#'#row_title_rot = switch(row_title_side[1], "left" = 90, "right" = 270),
#'#column_title = character(0),
#'#column_title_side = c("top", "bottom"),
#'#column_title_gp = gpar(fontsize = 14),
#'#column_title_rot = 0,
#'#cluster_rows = TRUE,
#'#clustering_distance_rows = "euclidean",
#'#clustering_method_rows = "complete",
#'#row_dend_side = c("left", "right"),
#'#row_dend_width = unit(10, "mm"),
#'#show_row_dend = TRUE,
#'#row_dend_reorder = TRUE,
#'#row_dend_gp = gpar(),
#'#row_hclust_side = row_dend_side,
#'#row_hclust_width = row_dend_width,
#'#show_row_hclust = show_row_dend,
#'#row_hclust_reorder = row_dend_reorder,
#'#row_hclust_gp = row_dend_gp,
#'#cluster_columns = TRUE,
#'#clustering_distance_columns = "euclidean",
#'#clustering_method_columns = "complete",
#'#column_dend_side = c("top", "bottom"),
#'#column_dend_height = unit(10, "mm"),
#'#show_column_dend = TRUE,
#'#column_dend_gp = gpar(),
#'#column_dend_reorder = TRUE,
#'#column_hclust_side = column_dend_side,
#'#column_hclust_height = column_dend_height,
#'#show_column_hclust = show_column_dend,
#'#column_hclust_gp = column_dend_gp,
#'#column_hclust_reorder = column_dend_reorder,
#'#row_order = NULL,
#'#column_order = NULL,
#'#row_names_side = c("right", "left"),
#'#show_row_names = TRUE,
#'#row_names_max_width = default_row_names_max_width(),
#'#row_names_gp = gpar(fontsize = 12),
#'#column_names_side = c("bottom", "top"),
#'#show_column_names = TRUE,
#'#column_names_max_height = default_column_names_max_height(),
#'#column_names_gp = gpar(fontsize = 12),
#'#top_annotation = new("HeatmapAnnotation"),
#'#top_annotation_height = top_annotation@size,
#'#bottom_annotation = new("HeatmapAnnotation"),
#'#bottom_annotation_height = bottom_annotation@size,
#'#km = 1,
#'#km_title = "cluster%i",
#'#split = NULL,
#'#gap = unit(1, "mm"),
#'#combined_name_fun = function(x) paste(x, collapse = "/"),
#'#width = NULL,
#'#show_heatmap_legend = TRUE,
#'#heatmap_legend_param = list(title = name),
#'#use_raster = FALSE,
#'#raster_device = c("png", "jpeg", "tiff", "CairoPNG", "CairoJPEG", "CairoTIFF"),
#'#raster_quality = 2,
#'#raster_device_param = list())


ms_plot_heatmap_per_subtype<- function(assay1_df = NULL,
                                   assay2_df = NULL,
                                   pheno_df = NULL,
                                   custom_annotations_list = NULL,
                                   heatmap_parameters_list = NULL,
                                   cluster_within_groups_by = NULL,
                                   sample_subset = NULL){
  library(ComplexHeatmap)
  library(circlize)
  
  ### define helper functions:
  
  #hierarchical clustering on rows of assay
  cluster_rows <- function(assay_df){
    if(nrow(assay_df)>1){
      bdist_rows <- stats::dist(x = assay_df, method = "euclidean")
      hierclust_rows <- hclust(d = bdist_rows, method = "complete")
    }else{
      hierclust_rows <- list(order = 1)
    }
    return(hierclust_rows)
  }
  
  #use column defined by 'cluster_within_groups_by' in pheno_df 
  #to order pheno_df (if assay_df=NULL) or assay_df
  sort_assays_by_group <- function(pheno_df, cluster_within_groups_by, assay_df=NULL){
    stopifnot(cluster_within_groups_by %in% colnames(pheno_df))
    if(!is.null(assay_df)){
      #transpose assay into observations (rows) x features (columns) data.frame
      assay_t_df <- as.data.frame(t(assay_df), drop=FALSE)
      assay_t_df <- assay_t_df[order(pheno_df[,cluster_within_groups_by]), ]
      return(assay_t_df)
    }
    pheno_df <- pheno_df[order(pheno_df[,cluster_within_groups_by]), ]
    return(pheno_df)
  }
  
  #use vector defining which sample belongs to which group to split assay_df into subgroups
  #cluster the samples in each subgroup and combine subgroup clustering back together
  cluster_within_subgroups <- function(assay_df, groups_vector){
    stopifnot(identical(nrow(assay_df), length(groups_vector)))
    groups_vector <- as.character(groups_vector) #to make sure it is not a factor, which may contain 0 samples/group
    assay_df_split_list <- split(assay_df, f = groups_vector)
    hierclust_columns <- list()
    for(group_i in seq_along(assay_df_split_list)){
      if(nrow(assay_df_split_list[[group_i]])>1){
        bdist <- stats::dist(x = assay_df_split_list[[group_i]], method = "euclidean")
        hierclust_columns[[group_i]] <- hclust(d = bdist, method = "complete")$order
      }else{
        hierclust_columns[[group_i]] <- 1
      }
    }
    #get the number of samples per subtype
    nr_obs_per_group <- sapply(hierclust_columns, length)
    #how many samples occur before subtype x?
    nr_obs_before_group <- c(0,  cumsum(nr_obs_per_group))
    #add the number of samples occuring before the clustering of the subtype x to the hierclust order
    #to keep the heatmap ordered by subtype while allowing clustering within the subtypes
    hierclust_columns <- as.vector(
      unlist(
        sapply(seq_along(hierclust_columns), function(x){
          hierclust_columns[[x]] + nr_obs_before_group[x]
        }))) 
    return(hierclust_columns)
  }
  
  ### check inputs:
  
  #check that at least 1 assay is provided
  if(is.null(assay1_df)){
    if(is.null(assay2_df)){
      stop("At least one of the following need to be supplied: assay1_df, assay2_df")
    }else{
      assay1_df <- assay2_df
      assay2_df <- NULL
    }
  }
  
  #check that colnames are set
  if(is.null(colnames(assay1_df))){
    stop("assay1_df needs to have unique colnames")
  }else if(!is.null(assay2_df) & is.null(colnames(assay2_df))){
    stop("assay2_df needs to have unique colnames")
  }
  
  #test if pheno_df was provided and whether rownames match with colnames of assay1_df
  if(is.null(pheno_df)){
    message("No phenoDf was provided, plotting heatmap without annotations")
    custom_annotations_list <- NULL
    pheno_df <- msInitializeDf(colNr = 1, rowNr = ncol(assay1_df), row.names=colnames(assay1_df))
  }else if(!identical(rownames(pheno_df), colnames(assay1_df))){
    stop("rownames of pheno_df need to match colnames of assay1_df")
  }
  
  #check that colnames match between assays
  if(!is.null(colnames(assay1_df)) & !is.null(colnames(assay2_df))){
    if(!identical(colnames(assay1_df), colnames(assay2_df))){
      stop('Colnames need to match between assay1_df and assay2_df')
    }
  }
  
  #check completeness of data
  if(anyNA(assay1_df)){
    stop("assay1_df contains NA values")
  }
  if(!is.null(assay2_df)){
    if(anyNA(assay2_df)){
      stop("assay2_df contains NA values")
    }
  }
  
  #check if data is numeric
  if(!is.numeric(as.matrix(assay1_df))){
    stop("assay1_df needs to contain numeric values")
  }
  #output of msNormFoldChange is data.frame, which does not return TRUE to is.numeric
  if(!is.null(assay2_df)){
    if(!is.numeric(as.matrix(assay2_df))){
      stop("assay2_df needs to contain numeric values")
    }
  }
  
  
  ### prepare data:
  
  #reduce data to the samples defined by `sample_subset`
  if(!is.null(sample_subset)){
    stopifnot(is.list(sample_subset) & length(sample_subset)==2)
    chosen_samples <- pheno_df[, sample_subset[[1]] ] %in% sample_subset[[2]]
    if(sum(chosen_samples)<3){
      stop('Less than 3 samples remain after filtering data by `sample_subset`')
    }
    assay1_df <- assay1_df[, chosen_samples]
    if(!is.null(assay2_df)){
      assay2_df <- assay2_df[, chosen_samples]
    }
    pheno_df <- pheno_df[chosen_samples, ]
  }

  ### cluster rows:
  
  message("Checks completed. Starting clustering")
  #clustering on the rows
  #do not perform row clustering if cluster_rows = FALSE in heatmap_parameters_list
  do_row_clustering <- TRUE
  if("cluster_rows" %in% names(heatmap_parameters_list)){
    if(!heatmap_parameters_list$cluster_rows){
      do_row_clustering <- FALSE
    }
  }
  
  if(do_row_clustering){
    hierclust_rows <- cluster_rows(assay1_df)
  }else{
    hierclust_rows <- list(order = 1:nrow(assay1_df))
  }

  ### cluster columns:
  
  #do not perform column clustering if cluster_columns = FALSE is set
  do_column_clustering <- TRUE
  if("cluster_columns" %in% names(heatmap_parameters_list)){
    if(!heatmap_parameters_list$cluster_columns){
      do_column_clustering <- FALSE
    }
  }
  
  #add pseudo-annotation to split assays by to ease calculation in case no annotation column is provided
  if(is.null(cluster_within_groups_by)){
    #assay1_df <- rbind(assay1_df, "pseudo_sort" = rep(" ", ncol(assay1_df)))
    pheno_df$pseudo_sort <- rep(" ", nrow(pheno_df))
    cluster_within_groups_by <- "pseudo_sort"
  }
  
  #reorder assay1_df and pheno_df by the annotation column (no reordering, if pseudo_sort column is used)
  #does also transpose assay1_df for subsequent clustering
  assay1_df <- sort_assays_by_group(pheno_df, cluster_within_groups_by, assay1_df)
  if(!is.null(assay2_df)){
    assay2_df <- sort_assays_by_group(pheno_df, cluster_within_groups_by, assay2_df)
  }
  pheno_df <- sort_assays_by_group(pheno_df, cluster_within_groups_by)

  if(do_column_clustering){
    #calculate clustering order in each subgroup and save their order in a vector
    hierclust_columns <- cluster_within_subgroups(assay1_df, pheno_df[, cluster_within_groups_by])
  }else{
    hierclust_columns <- 1:nrow(assay1_df)
  }
  
  #change the sample order in the assays and pheno data based on row and column clustering
  pheno_df <- pheno_df[hierclust_columns, ]
  assay1_df <- as.data.frame(t(assay1_df)[hierclust_rows$order, hierclust_columns, drop=FALSE])
  if(!is.null(assay2_df)){
    assay2_df <- as.data.frame(t(assay2_df)[hierclust_rows$order, hierclust_columns, drop=FALSE]) 
  }
  message("Finished clustering on assay1_df")
  
  ### set fixed heatmap parameters:

  assay1_heatmap_parameters <- list(name="DNA methylation",
                                    col = colorRamp2(c(0,0.5,1), c("blue","white", "red")),
                                    column_title = "DNA methylation",
                                    column_title_gp = gpar(fontsize = 10),
                                    column_names_gp = gpar(fontsize = 8),
                                    row_names_gp = gpar(fontsize = 8),
                                    row_title_gp = gpar(fontsize = 10),
                                    cluster_columns = FALSE,
                                    cluster_rows = FALSE,
                                    column_dend_reorder = FALSE,
                                    show_column_names = TRUE,
                                    show_row_names = FALSE,
                                    show_heatmap_legend = T,
                                    width= unit(15, "cm"))
  
  assay2_heatmap_parameters <- list(name="RNA expression",
                                    #every value > 2 is just yellow
                                    #col = colorRamp2(c(0,0.5,1,1.5,2),c("black","blue", "white", "yellow", "orange"))
                                    #wider range until 6
                                    col = colorRamp2(c(0,0.5,1,2,4,8), c("black","blue", "white", "yellow", "orange", "red")),
                                    column_title = "RNA expression",
                                    column_title_gp = gpar(fontsize = 10),
                                    row_title_gp = gpar(fontsize = 10),
                                    column_names_gp = gpar(fontsize = 8),
                                    row_names_gp = gpar(fontsize = 8),
                                    cluster_columns = FALSE,
                                    cluster_rows = FALSE,
                                    column_dend_reorder = FALSE,
                                    show_column_names = TRUE,
                                    show_row_names = TRUE,
                                    width= unit(15, "cm"),
                                    heatmap_legend_param = list(
                                      show_heatmap_legend = T,
                                      at = c(0,0.5, 1, 2, 3, 4,5,6,7, 8) #at = seq(0,2,0.5),
                                    ))

  ### create the heatmapAnnotations:
  
  if(!is.null(custom_annotations_list)){
    message('Adding heatmap annotations from custom_annotations_list')
    heatmap_annot <- msHeatmapAnnotations(
      phenoDf = pheno_df,
      annotList = custom_annotations_list,
      legendPos = ifelse('annotations_position' %in% names(heatmap_parameters_list), 
                         heatmap_parameters_list$annotations_position, 
                         TRUE),
      annSizeCm = ifelse('annotation_size_cm' %in% names(heatmap_parameters_list), 
                         heatmap_parameters_list$annotation_size_cm, 
                         0.1),
      naCol = ifelse('annotation_na_col' %in% names(heatmap_parameters_list), 
                     heatmap_parameters_list$annotation_na_col, 
                     'grey95'))
    

    #add the heatmap annotations to the general heatmap parameters list
    assay1_heatmap_parameters <- c(assay1_heatmap_parameters,
                                   list(top_annotation = heatmap_annot$ha_top,
                                        bottom_annotation = heatmap_annot$ha_bottom))
    assay2_heatmap_parameters <- c(assay2_heatmap_parameters,
                                   list(top_annotation = heatmap_annot$ha_top,
                                        bottom_annotation = heatmap_annot$ha_bottom))
  }else{
    assay1_heatmap_parameters <- c(assay1_heatmap_parameters, 
                                   list(top_annotation = NULL, bottom_annotation=NULL))
    assay2_heatmap_parameters <- c(assay2_heatmap_parameters, 
                                   list(top_annotation = NULL, bottom_annotation=NULL))
  }

  ### modify heatmap parameters by user input:
  
  #if custom options are provided for the heatmap function by the user, replace the default specifications
  if(!is.null(heatmap_parameters_list)){
    #set extra parameters to NULL to avoid interfering with Heatmap call
    heatmap_parameters_list$annotation_position <- NULL
    heatmap_parameters_list$annotation_size_cm <- NULL
    heatmap_parameters_list$annotation_na_col <- NULL
    message('Adding custom parameters from heatmap_parameters_list')
    for(usr_optn in names(heatmap_parameters_list)){
      assay1_heatmap_parameters[[usr_optn]] <- heatmap_parameters_list[[usr_optn]]
      assay2_heatmap_parameters[[usr_optn]] <- heatmap_parameters_list[[usr_optn]]
    }
  }
  #since clustering of rows and columns is separately done within this function
  #make sure Heatmap() function call does not perform a second clustering on top
  assay1_heatmap_parameters[['cluster_rows']] <- FALSE
  assay1_heatmap_parameters[['cluster_columns']] <- FALSE
  assay2_heatmap_parameters[['cluster_rows']] <- FALSE
  assay2_heatmap_parameters[['cluster_columns']] <- FALSE
  
  ### construct the heatmaps by calling Heatmap() from ComplexHeatmap package:
  
  message('Calling Heatmap for assay1_df')
  h1 <- do.call(Heatmap, c(list(matrix = as.matrix(assay1_df)), assay1_heatmap_parameters))
  
  if(!is.null(assay2_df)){
    message('Calling Heatmap for assay2_df')
    h2 <- do.call(Heatmap, c(list(matrix = as.matrix(assay2_df)), assay2_heatmap_parameters))
  }else{
    h2 <- NULL
  }

  heatmap_combined <- list(assay1_heatmap=h1, assay2_heatmap=h2)
  return(heatmap_combined)
}
