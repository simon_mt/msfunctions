#' Function to extract legend
#' taken from https://stackoverflow.com/questions/12041042/how-to-plot-just-the-legends-in-ggplot2
#' @param ggPlot ggplot object
#' @param fileName filename of the plots passed to msSavePlot, additional arguments can also be passed.
#' @export

msSaveLegend <- function(ggPlot, fileName, ...){
    library(grid)
    tmp <- ggplot_gtable(ggplot_build(ggPlot))
    leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
    legend <- tmp$grobs[[leg]]
    msSavePlot(legend, filename = fileName, ...)
}
